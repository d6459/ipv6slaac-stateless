
!Difference ipv6 enable and ipv6 unicast-routing
!Without ipv6 unicast-routing, the router behaves as an IPv6 host, not as an IPv6 router.
!The ipv6 enable command on an interface configured alone has two primary functions: It allows the interface to send and receive IPv6 packets, and automatically assigns an IPv6 link-local unicast address to the interface, based on the Modified EUI-64. Configuring ipv6 enable on an interface that already has another IPv6 address configured has no effect since that interface is already IPv6-enabled, and certainly has its link-local address already. In other words, whenever you configure an explicit unicast IPv6 address on an interface, whether link-local or any other (global, unique local, ...), the router will also implicitly enable the functionality of ipv6 enable even though that command won't be added to the configuration.

 

!R1
en
conf t
h R1

ipv6 route 2001:db8:aaaa:f::/64 2001:db8:aaaa:b::2

ipv6 unicast-routing

int g0/0/0
ipv6 add fe80::1 link-local
ipv6 add 2001:db8:aaaa:a::/64 eui-64
no sh

int g0/0/1
ipv6 add fe80::2 link-local
ipv6 add 2001:db8:aaaa:b::1/64
no sh

end

!---------------------------------------------


!R2
en
conf t
h R2

ipv6 route 2001:db8:aaaa:a::/64 2001:db8:aaaa:b::1

ipv6 unicast-routing

ipv6 dhcp pool DHCPv6-LIST
dns-server 2001:CAFE::1
domain-name dominion.lan

int g0/0/0
ipv6 add fe80::1 link-local
ipv6 add 2001:db8:aaaa:f::/64 eui-64
ipv6 nd other-config-flag
ipv6 dhcp server DHCPv6-LIST
no sh

int g0/0/1
ipv6 add fe80::2 link-local
ipv6 add 2001:db8:aaaa:b::2/64
no sh

end
